package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.repository.dto.IAbstractUserOwnedModelDtoRepository;
import ru.t1.shevyreva.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedModelDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractModelDtoRepository<M>
        implements IAbstractUserOwnedModelDtoRepository<M> {

    public AbstractUserOwnedModelDtoRepository(EntityManager entityManager) {
        super(entityManager);
    }

    public void addForUser(@NotNull String userId, @NotNull M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void updateForUser(@NotNull String userId, @NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public abstract void clearForUser(@NotNull String userId);

    @Override
    public abstract void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract List<M> findAllForUser(@NotNull String userId);

    @Override
    public abstract M findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public abstract int getSizeForUser(@NotNull String userId);

}
