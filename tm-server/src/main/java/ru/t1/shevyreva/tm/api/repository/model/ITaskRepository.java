package ru.t1.shevyreva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedModelRepository<Task> {

    void clearForUser(@NotNull String userId);

    void clear();

    List<Task> findAllForUser(@NotNull String userId);

    List<Task> findAll();

    List<Task> findAll(@Nullable Comparator<Task> comparator);

    List<Task> findAllForUser(@NotNull String userId, @Nullable Comparator<Task> comparator);

    Task findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable Task findOneById(@NotNull String id);

    Task findOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    Task findOneByIndex(@NotNull Integer index);

    int getSize();

    int getSizeForUser(@NotNull String userId);

    void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    void removeOneById(@NotNull String id);

    boolean existsByIdForUser(@NotNull String userId, @NotNull String id);

    boolean existById(@NotNull String id);

    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
