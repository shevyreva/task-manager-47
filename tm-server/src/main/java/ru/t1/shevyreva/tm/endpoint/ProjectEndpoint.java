package ru.t1.shevyreva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.dto.IProjectDtoService;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.request.project.*;
import ru.t1.shevyreva.tm.dto.response.project.*;
import ru.t1.shevyreva.tm.enumerated.ProjectDtoSort;
import ru.t1.shevyreva.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectDtoService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final ProjectDTO project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        @Nullable final Integer index = request.getIndex();
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        getProjectService().removeAll(userId);
        return new ProjectClearResponse();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final ProjectDTO project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final ProjectDtoSort sort = request.getSort();
        @NotNull final List<ProjectDTO> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = getProjectService().removeOneById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String id = request.getId();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
