package ru.t1.shevyreva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.model.User;

public interface IUserRepository extends IAbstractModelRepository<User> {

    User findByLogin(@NotNull final String login);

    User findByEmail(@NotNull final String email);

    void removeOne(@NotNull final User user);

}
