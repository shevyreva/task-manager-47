package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.data.DataSaveYamlFasterXmlRequest;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save Data to yaml file.";

    @NotNull
    private final String NAME = "data-save-yaml";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataSaveYamlFasterXmlRequest request = new DataSaveYamlFasterXmlRequest(getToken());
        getDomainEndpoint().saveYamlDataFasterXml(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
